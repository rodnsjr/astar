# Assignment Questions / Answers

## 1. Explain briefly how you implemented the datatype for states.
A simple *State* class with the game board and the heuristic values, in this case manhattan distance.

```java

	public class State{
		int[][] board;
		int manhattanDistance;
	
		void calcManhattanDistance();
	}

```

## 2. Explain briefly how you represented a search node (state + number of moves + previous search node).
A *Node* class with the current game state, the number of moves (as the cost) and the parent node.

```java
	
	public class Node{
		int state;
		int cost;
		Node parent;
	}

```


## 3. Explain briefly how you detected unsolvable puzzles.
To detect an unsolvable puzzle I've used the odd parity between the number of inversions of the start state with respect to the goal state and the manhattan distance of the empty square. 
For instance:
```


    initial:  2, 8, 3 1,6,4 7,0,5 
        goal: 1,2,3 8,0,4 7,6,5
    The number of inversions is: 9
    The manhattan distance is of the empty square is: 1
    Since both are odd parities the puzzle is solvable.


```

## 4. If you wanted to solve random 4-by-4 or 5-by-5 puzzles, which would you prefer: more time (say, 2x as much), more memory (say 2x as much), a better priority queue (say, 2x as fast), or a better priority function (say, one on the order of improvement from Hamming to Manhattan)? Why?
A better priority function, because the algorithm works on the idea of a "consistent heuristic", since there's always 3 to 4 possible moves in the game choosing the best move and discarding all others will always be optimal.

##5. If you did the extra credit, describe your algorithm briefly and state the order of growth of the running time (in the worst case) for isSolvable().
My algorithm is based on the odd parity between the number of inversions and the manhattan distance of the empty square. 
The idea behind it is to "sort" the array from initial state to the goal state, counting the inversions. The growth of the running time will be exponential to the size of the array, and the number of inversions, since the algorithm will be "sorting" the entire array 1 step at a time.

## 6. Known bugs / limitations
I have'nt tested on a broader range of problems, and puzzles. But I've added a few other "harder to solve" problems.

## 7. Describe whatever help (if any) that you received. Don�t include readings, lectures, and precepts, but do include any help from people (including staff, classmates, and friends) and attribute them by name.
A coworker *Alexandre* helped me with the algorithm to detect the unsolvable puzzle. And another friend *Guilherme* gave me some tips for the algorithm and some links too. Besides them, the almighty *Stackoverflow* programmer's friend!

## 8. Describe any serious problems you encountered.

## 9. List any other comments here. Feel free to provide any feedback on how much you learned from doing the assignment, and whether you enjoyed doing it.
The assignment helped me to understand how easy is to implement some of the major AI algorithms.
I had some minor difficulties at first: understanding the heuristics, and the algorithm. But with the help of the content provided by the class, the AIMA book, and the internet community everything went clear.
I've also learned a little bit more about some of the Java data structures, such as PriorityQueue, HashSets, and TreeSets.
Also changing the *openSet* to a PriorityQueue improved the speed from 30.000MS to 9.000MS.



### A* pseudocode used

```
#!java

1  Create a node containing the goal state node_goal  
2  Create a node containing the start state node_start  
3  Put node_start on the open list  
4  while the OPEN list is not empty  
5  {  
6  Get the node off the open list with the lowest f and call it node_current  
7  if node_current is the same state as node_goal we have found the solution; break from the while loop  
8      Generate each state node_successor that can come after node_current  
9      for each node_successor of node_current  
10      {  
11          Set the cost of node_successor to be the cost of node_current plus the cost to get to node_successor from node_current  
12          find node_successor on the OPEN list  
13          if node_successor is on the OPEN list but the existing one is as good or better then discard this successor and continue  
14          if node_successor is on the CLOSED list but the existing one is as good or better then discard this successor and continue  
15          Remove occurences of node_successor from OPEN and CLOSED  
16          Set the parent of node_successor to node_current  
17          Set h to be the estimated distance to node_goal (Using the heuristic function)  
18           Add node_successor to the OPEN list  
19      }  
20      Add node_current to the CLOSED list  
21  }
```