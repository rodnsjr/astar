package astar.sample;

public class Config {
	public static int[][] goal;
	public static int[] singleArrayGoal;
	public static int PUZZLE_SIZE;
	
	public static int[] getSingleArrayGoal(){
		if (singleArrayGoal == null){
			singleArrayGoal = new int[goal.length*goal[0].length];
			int index=0;
			for (int[] is : goal) {
				for (int i : is) {
					singleArrayGoal[index++] = i;
				}
			}
		}
		return singleArrayGoal;
	}
	
	public static int[] getSingleArrayBoard(int[][] board){
		int[] singleArray = new int[board.length*board[0].length];
		int index=0;
		for (int[] is : board) {
			for (int i : is) {
				singleArray[index++]=i;
			}
		}
		return singleArray;
	}
}
