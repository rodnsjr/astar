package astar.sample;
import astar.main.Solver;
import astar.main.TestSamples;

public class Test {
	public static void main(String[] args) {		
		Solver solver = new Solver();
		
		String[]exit=solver.solve(TestSamples.mediumStart, TestSamples.mediumGoal);
		
		System.out.println(exit.length);
		for (String string : exit) {
			System.out.println(string);
		}
		
	}
}
