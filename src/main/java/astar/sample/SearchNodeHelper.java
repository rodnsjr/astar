package astar.sample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SearchNodeHelper {
	/**
	 * Get the path to this node
	 * @param node
	 * @return
	 */
	public static Collection<String> getPath(SearchNode node){
		
		SearchNode tempNode = node;
		List<String> path = new ArrayList<>();
		do {
			
			path.add(tempNode.getCurState().getMove());
			tempNode = tempNode.getParent();
			
		} while (tempNode.getParent() != null);
		
		return path;
	}
}
