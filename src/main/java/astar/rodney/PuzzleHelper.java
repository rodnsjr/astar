package astar.rodney;
import java.awt.Point;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

public class PuzzleHelper {

    static final String LEFT = "left";
    static final String RIGHT = "right";
    static final String UP = "up";
    static final String DOWN = "down";

    public static final Collection<Node> generateNodeSucessors(Node currentTreeNode){

        HashSet<Node> nodes = new HashSet<>();

        int[][] currentBoard = currentTreeNode.getState().getBoard();
        Point currentBlankPosition = getPosition(currentBoard);

        for (String legalMove : findLegalMoves(currentBlankPosition, 
                currentBoard.length, currentBoard[0].length)) {

            int[][] newBoard = generateBoard(legalMove, currentBoard, currentBlankPosition);
            
            State state = new State(newBoard);
            //Add the new node, add +1 to the cost 
            nodes.add(new Node(currentTreeNode, state, 
                    currentTreeNode.getCost()+1, legalMove));

        }

        return nodes;
    }

    private static final int[][] generateBoard(String side, int[][] currentBoard, Point blankPosition){

        if (side == LEFT){
            int[][] newBoard = getClonedBoard(currentBoard);

            int swap = newBoard[blankPosition.y][blankPosition.x - 1];
            newBoard[blankPosition.y][blankPosition.x - 1] = 0;
            newBoard[blankPosition.y][blankPosition.x] = swap;

            return newBoard;

        }else if (side == RIGHT){
            int[][] newBoard = getClonedBoard(currentBoard);

            int swap = newBoard[blankPosition.y][blankPosition.x + 1];
            newBoard[blankPosition.y][blankPosition.x + 1] = 0;
            newBoard[blankPosition.y][blankPosition.x] = swap;

            return newBoard;

        }else if (side == UP){
            int[][] newBoard = getClonedBoard(currentBoard);

            int swap = newBoard[blankPosition.y - 1][blankPosition.x];
            newBoard[blankPosition.y - 1][blankPosition.x] = 0;
            newBoard[blankPosition.y][blankPosition.x] = swap;

            return newBoard;


        }else if (side == DOWN){
            int[][] newBoard = getClonedBoard(currentBoard);

            int swap = newBoard[blankPosition.y + 1][blankPosition.x];
            newBoard[blankPosition.y + 1][blankPosition.x] = 0;
            newBoard[blankPosition.y][blankPosition.x] = swap;

            return newBoard;

        }

        return null;
    }

    private static int[][] getClonedBoard(int[][] currentBoard) {
        int[][] clonedBoard = new int[currentBoard.length][currentBoard[0].length];

        for (int i = 0; i < currentBoard.length; i++) {
            for (int j = 0; j < currentBoard[i].length; j++) {
                clonedBoard[i][j] = currentBoard[i][j];
            }
        }

        return clonedBoard;
    }

    private static final Collection<String> findLegalMoves(Point position, int boardHeight, int boardWidth){

        HashSet<String> legalMoves = new HashSet<>();
        //Y n�o esta no topo
        if (position.y != 0){
            legalMoves.add(UP);
        }
        //Y n�o est� no fundo
        if (position.y < boardHeight -1)
            legalMoves.add(DOWN);


        if (position.x != 0)
            legalMoves.add(LEFT);

        if (position.x < boardWidth -1)
            legalMoves.add(RIGHT);

        return legalMoves;
    }

    private static final Point getPosition(int[][] board){
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[y].length; x++) {
                if (board[y][x]==0){
                    return new Point(x, y);
                }
            }
        }
        return null;
    }

    public static final int calcManhattanOfNumber(int[][] s0, int[][]g, int number){
        Point p0 = findPositions(s0, number);
        Point pg = findPositions(g, number);

        int manhattan = Math.abs(Math.abs(p0.x-pg.x) + Math.abs(p0.y-pg.y));
        return manhattan;
    }

    public static final Point findPositions(int[][] state, int number){
        for (int y1 =0; y1 < state.length; y1++){
            for (int x1 =0; x1< state[y1].length; x1++){
                if (state[y1][x1]==number){
                    return new Point(x1,y1);
                }    
            }
        }
        return null;
    }
    
    public static final int countInversions(int[] s0, int[] goal){
        //Pega o indice do goal
        int goalIndex = 0;
        //Contar invers�es
        int inversionCount = 0;
        int[] answer = s0;
        
        while (!Arrays.equals(answer, goal)) {
          int currentNumber = goal[goalIndex];
          
          //Busca no estado atual a posi��o do item do indice goalIndex
              for (int i = goalIndex; i < goal.length; i++) {
                  if (answer[i] == currentNumber && i != goalIndex) {
                      //Encontrando-o muda a posi��o dele em -1
                      int aux = answer[i - 1];
                      answer[i-1] = answer[i];
                      answer[i] = aux;
                      //Quebra o lasso de busca
                      inversionCount++;
                      break;
                  }else if (answer[i] == currentNumber && i == goalIndex){
                       goalIndex++;
                       break;
                  }
              }
        }

        return inversionCount;
    }
    
    public static final int[] cloneArray(int[] array){
        int[] clone = new int[array.length];
        
        for (int i = 0; i < clone.length ; i++) {
            clone[i] = array[i];
        }
        
        return clone;
    }
    
    public static final int[] toArray(int[][] array){
        int[] exitArray = new int[array.length*array[0].length];
        int index=0;
        for (int[] is : array) {
            for (int i : is) {
                exitArray[index++]=i;
            }
        }
        
        return exitArray;
    }
    

}
