package astar.rodney;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Node implements Comparable<Node>{
	private Node parent;
	
	private String action;
	
	private int branchSize;
	
	private State state;
	
	private int cost;
	private int hCost;
		
	public Node(){
		branchSize = 0;
	}
	
	public Node(State state, int cost){
	    this();
	    this.cost=cost;
	    this.state=state;
	}
		
	public Node(Node parent, State state, int cost, String action) {
		this(state, cost);
		this.parent = parent;
		this.action = action;
		
		if (parent!=null)
			this.branchSize = parent.getBranchSize()+1;
		else
			this.branchSize=0;
		
		this.state = state;
		this.hCost = state.getManDist();
	}
	
	public String getAction() {
		return action;
	}
	
	public Node getParent() {
		return parent;
	}
	
	public int getBranchSize() {
		return branchSize;
	}
	   
	public State getState() {
	    return state;
	}
	
	public int getCost() {
        return cost;
    }
	
	public int getHeuristicCost() {
        return hCost;
    }
	
	public int getFullCost(){
	    return cost+hCost;
	}

	@Override
	public boolean equals(Object obj) {
	    Node node = (Node) obj;
	    return node.getState().equals(this.getState());
	}
	
	@Override
	public String toString() {
	    return String.format("%s\n%s", branchSize, state);
	}

	/**
	 * Get the path to achieve to this Branch
	 * @param branch
	 * @return
	 */
	public static final String[] getPath(Node branch){
	    List<String> path = new ArrayList<>();
	    
	    Node iteratorBranch = branch;
	    
	    while (iteratorBranch.getParent()!=null) {
	    	path.add(iteratorBranch.action);
	    	iteratorBranch=iteratorBranch.getParent();
	    }
	    
	    Collections.reverse(path);
	    
	    if (path.isEmpty())
	    	return new String[]{ null };
	    	
	    return path.toArray(new String[path.size()]);
	}

	@Override
	public int compareTo(Node o) {
		
		if (getFullCost()<o.getFullCost()){
			return -1;
		}else if (getFullCost()==o.getFullCost()){
			return 0;
		}		
		
		return 1;
	}
	
}
