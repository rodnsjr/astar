package astar.rodney;
import java.util.Collection;
import java.util.HashSet;
import java.util.PriorityQueue;

public class AStar {
	
	private int iterationsCount;

	public AStar() {
		iterationsCount = 0;
	}

	public Node solve(int[][] board, int[][] goal){

		Node firstNode = new Node(new State(board), 0);
		HashSet<Node> closedSet = new HashSet<>();
		PriorityQueue<Node> openSet = new PriorityQueue<>();
		openSet.add(firstNode);
		Node treeNode = null;

		while (!openSet.isEmpty()) {
			
			iterationsCount++;
			
			treeNode = openSet.poll();
			
			if(!treeNode.getState().isBoard(goal)){
				
				closedSet.add(treeNode);
				
				Collection<Node> nodesSucessors = PuzzleHelper
						.generateNodeSucessors(treeNode);
								
				for (Node newNode : nodesSucessors) {
					//Ignorar os que j� foram descobertos
					if (!containsNode(closedSet, newNode)){
						openSet.add(newNode);
					}
				}
			}else{
				break;
			}

		}
		
		return treeNode;

	}
	
	public int getMinMoves() {
		return iterationsCount;
	}
	
	public Node getTheLowestCostBranch(Collection<Node> branches){
		Node lowest = null;
		for (Node treeBranch : branches) {
			if (lowest == null)
				lowest = treeBranch;
			else if (treeBranch.getFullCost() < lowest.getFullCost())
				lowest = treeBranch;
		}
		return lowest;
	}
	
	public boolean containsNode(Collection<Node>list, Node node){
		
		for (Node treeNode : list) {
			if (treeNode.getState().isBoard(node.getState().getBoard())){
				return true;
			}
		}
		
		return false;
	}

}
