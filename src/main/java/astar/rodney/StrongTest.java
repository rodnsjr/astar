package astar.rodney;

import astar.main.Solver;
import astar.main.TestSamples;

public class StrongTest {
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		
		Solver solver = new Solver();
		
		String[] path = solver.solve(TestSamples.mediumStart, TestSamples.mediumGoal);
		
		solver.setSolved(false);
		String[] path1 = solver.solve(TestSamples.hardStart, TestSamples.hardGoal);
		
		solver.setSolved(false);
		String[] path2 = solver.solve(TestSamples.hardStart2, TestSamples.hardGoal2);
		
		System.out.println("medium  " + path.length);
		
		System.out.println("hard1 " + path1.length);
		
		System.out.println("hard2 " + path2.length);
		
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println(elapsedTime);
	}
}
