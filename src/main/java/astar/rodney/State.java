package astar.rodney;

import java.util.Arrays;

public class State {
    private int manDist;
    private int[][] board;
    
    //Not in use ATM
    //private int outOfPlace;
    //public static int[][] goal;
    
    public State(int[][] board) {
        this.board = board;
        calcManhattanDistance();
    }
    
    public int getManDist() {
        return manDist;
    }
    
    public int[][] getBoard() {
        return board;
    }
    
    /*rodney
    private void calcManhattanDistance() {
        int manhattanDistanceSum = 0;
        for (int[] is : board) {
            for (int i : is) {
                if (i!=0)
                    manhattanDistanceSum+= PuzzleHelper.calcManhattan(board, goal, i);
            }
        }
        this.manDist = manhattanDistanceSum;
    }*/
    
    /*google*/
    private void calcManhattanDistance() {
        int manhattanDistanceSum = 0;
        for (int y = 0; y < board.length; y++) 
            for (int x = 0; x < board[y].length; x++) { 
                int value = board[x][y]; 
                if (value != 0) { 
                    int targetX = (value - 1) / board[y].length; // expected x-coordinate (row)
                    int targetY = (value - 1) % board.length; // expected y-coordinate (col)
                    int dx = x - targetX; // x-distance to expected coordinate
                    int dy = y - targetY; // y-distance to expected coordinate
                    manhattanDistanceSum += Math.abs(dx) + Math.abs(dy); 
                } 
            }
        this.manDist = manhattanDistanceSum;
    }
    
    /*
    public void calcOutOfPlace(){
        for (int y = 0; y < board.length; y++)
        {
            for (int x = 0; x < board[y].length; x++)
            {
                if (board[y][x] != goal[y][x])
                    this.outOfPlace +=1;
            }
        }
    }
    */
    
    public boolean isBoard(int[][] boardCompare){
        return Arrays.deepEquals(this.board, boardCompare);
    }
    
    
    @Override
    public boolean equals(Object obj) {
        State node = (State) obj;
        
        return Arrays.deepEquals(node.board, this.board);
    }
    
    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        
        for (int y = 0; y < board.length; y++) {
            
            for (int x = 0; x < board[y].length ; x++) {
            
                toStringBuilder.append(""+board[y][x]);
            
            }
            
            toStringBuilder.append("\n");
        }
        
        return toStringBuilder.toString();
    }
}
