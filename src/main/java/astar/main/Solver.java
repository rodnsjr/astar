package astar.main;

import astar.rodney.AStar;
import astar.rodney.PuzzleHelper;
import astar.rodney.Node;

/**
 * A search algorithm for the N-Puzzle
 * 2015-03-18
 * @author meneguzzi
 *
 */
public class Solver {

    private boolean isSolved = false;

    private AStar aStar = new AStar();

    private Node lastSolvedBranch;

    /**
     * Uses a search algorithm to generate a solution to the N-Puzzle starting from state <code>s0</code> to 
     * reach goal state <code>g</code>. If there is a solution, return an array of {@link String}s listing the moves, 
     * if the solution is trivial (no moves), return an array with 1 <code>null</code> {@link String}, otherwise 
     * (no solution), return <code>null</code>.
     * return an array with . 
     * @param s0 The initial state of the search problem
     * @param g The goal state of the search problem
     * @return a list of actions [a<sub>0</sub>, ..., a<sub>n</sub>] where each a<sub>i</sub> &in; {"up","down","left","right"}.
     */
    public String[] solve(int [][]s0, int [][]g) {
        if (!isSolved){
            if (isSolvable(s0, g)){

                Node branch = aStar.solve(s0, g);
                this.lastSolvedBranch = branch;

                isSolved=true;

                return Node.getPath(branch);

            }
        }
        return null;
    }

    /**
     * Returns whether or not the specified problem is solvable.
     * @param s0
     * @param g
     * @return
     */
    public boolean isSolvable(int [][]s0, int [][]g) {

        // Count inversions in given 8 puzzle
        int invCount = PuzzleHelper.countInversions(PuzzleHelper.toArray(s0)
                , PuzzleHelper.toArray(g));
        // Calc the manh of the blank values
        int manhBlank = PuzzleHelper.calcManhattanOfNumber(s0, g, 0);

        // return true if inversion count is even.
        return (((invCount+manhBlank)%2) == 0);
    }

    /**
     * Returns the maximum height of the search tree (i.e. the longest move sequence considered during search).
     * @param s0
     * @param g
     * @return
     */
    public int maxTreeHeight(int [][]s0, int [][]g) {
        if (isSolved)
            return lastSolvedBranch.getBranchSize();
        return solve(s0, g).length;
    }

    /**
     * Returns the minimum number of moves to solve the specified problem, or -1 if the problem is unsolvable.
     * @param s0
     * @param g
     * @return
     */
    public int minMoves(int [][]s0, int [][]g) {
        if (isSolvable(s0, g)){
            if (!isSolved){
                solve(s0, g);
            }
            return lastSolvedBranch.getBranchSize();
        }
        else
            return -1;
    }

	public void setSolved(boolean b) {
		this.isSolved = false;
	}
}
