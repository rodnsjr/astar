package astar.main;

public class TestSamples {
	
	public static final int[][] trivialStart = new int [][] {{0,1,2},{3,4,5},{6,7,8}};
	public static final int[][] trivialGoal =  new int [][] {{0,1,2},{3,4,5},{6,7,8}};
	
	public static final int[][] easyStart =  new int [][] {{7,2,4},{5,0,6},{8,3,1}};
	public static final int[][] easyGoal =   new int [][] {{7,2,4},{5,6,0},{8,3,1}};
	
	public static final int[][] mediumStart = new int [][] {{2,5,6},{0,1,3},{7,4,8}};
	public static final int[][] mediumGoal =  new int [][] {{1,2,3},{4,5,6},{7,8,0}};
	
	public static final int[][] hardStart = new int[][] {{8,6,7}, {2,5,4}, {3,0,1}};
	public static final int[][] hardGoal =  new int [][] {{1,2,3},{4,5,6},{7,8,0}};
	
	public static final int[][] hardStart2 = new int[][] {{6,4,7},{8,5,0},{3,2,1}};
	public static final int[][] hardGoal2 =  new int [][] {{1,2,3},{4,5,6},{7,8,0}};
}
